
/* 
 * File: sensors.c
 * Author: Jure Macerl
 * Comments: This file implements sensor reading and storing data into memory for later retrieval
 * Revision history: 1.0
 */

#include <xc.h>
#include "sensors.h"
#include "mcc_generated_files/spi2.h"



#define SPI_RX_IN_PROGRESS 0x00

SENSOR_OBJECT sensors[SENSOR_NUMBER];  // storage for all sensors without data buffers
//SENSOR_DATA sensor_data;//[SENSOR_NUMBER];  // buffer for all sensor readouts
READ_SENS_OBJECT  read_sens_object;  // reading object
SENSOR_MEM sensor_mem[SENSOR_NUMBER]; // buffer for all sensor readouts
READ_STATE  read_state = READ_DONE; // reading state data
PARSE_STATUS parse_status = PARSE_DONE; // parse done




// Initialize sensor space, pointers


uint8_t SensorRead(){

     static uint8_t i = 0;
     read_sens_object.object_ptr = &sensors[i];
     SENSOR_OBJECT *sens = read_sens_object.object_ptr;
     
     
    for (i = 0; i < SENSOR_NUMBER; i++) {



        if (sens->sens_conf.state == SENSOR_CONNECTED || sens->sens_conf.state == SENSOR_WORKING) {

            
            if (sens->sens_conf.PP == 0x00) {
                
                SPI_CHANGE_STATE(sens);
                read_state = SENSOR_GET_DATA_SPI(sens);  // current read state
                if(read_state == READ_DONE){
                    
                }
                else{
                sens->sens_conf.state = SENSOR_FAILED;
                }
                    
            } 
            else if (sens->sens_conf.PP == 0x01) {

                read_state = SENSOR_GET_DATA_I2C(sens);
                if(read_state == READ_DONE){
                    
                }
                else{
                sens->sens_conf.state = SENSOR_FAILED;
                }
            }
            else {

            }
        }

    else {

        sens++; // go to next sensor

    }

    return 1;
}

}

/**
 * 
 * Dodati je potrebno se tajmer ki spremlja ?as branja. ?e se ne prebere v dolo?enem ?asu je potrebno deaktivirati senzor 
 * in sporociti v bazo
 */
READ_STATE SENSOR_GET_DATA_SPI(SENSOR_OBJECT *sensor)
{
    read_state = READ_PENDING; // set state to pending
    
    SLAVE2_SELECT = sensor->sens_conf.SPI_ACTIVE;
    
  
    while(*(sensor->sens_conf.data_prt.readout_str_ptr) != 0x45){
         
     switch(*(sensor->sens_conf.data_prt.readout_str_ptr++)){
        
        case 0x41:
            SPI2_Exchange8bit(*(sensor->sens_conf.data_prt.readout_str_ptr++));
            SPI2_Exchange8bitBuffer(NULL,  *(sensor->sens_conf.data_prt.readout_str_ptr), sensor->sens_conf.data_prt.readout_ptr);
            sensor->sens_conf.data_prt.readout_ptr += *(sensor->sens_conf.data_prt.readout_str_ptr++);
            break;
            
        case 0x42:
            SPI2_Exchange8bit(*(sensor->sens_conf.data_prt.readout_str_ptr++));
            *(sensor->sens_conf.data_prt.readout_ptr++) = SPI2_Exchange8bit(DUMMY_DATA);
            break; 
            
        default:
            SPI2_Exchange8bit(*(sensor->sens_conf.data_prt.readout_str_ptr++)); // DEBUGGING PURPOSE
            break;
             
       
    }
     
    }
     
    SLAVE2_SELECT = ~(sensor->sens_conf.SPI_ACTIVE);  // disable slave
     
    //read_state = READ_DONE; // reading state data  
    return READ_DONE;
}

SENSOR_STATE SENSOR_GET_DATA_I2C(SENSOR_OBJECT *sensor)
{
    
  
    
}


void SPI_CHANGE_STATE(SENSOR_OBJECT *sensor)
{
    // Set the SPI2 module to the options selected in the User Interface
    
    // SMP Middle; CKE Idle to Active; 
    SSP2STATbits.SMP = sensor->sens_conf.SMP ;
    SSP2STATbits.CKE = sensor->sens_conf.CPHA;
    
    // SSPEN enabled; WCOL collision; CKP Idle:Low, Active:High; SSPM FOSC/64; SSPOV no_overflow; 
    SSP2CON1bits.CKP = sensor->sens_conf.CPOL;
    
    // SSP2ADD 0; 
    SSP2ADD = 0x00;
    
    TRISD = TRISD & 0x7F;
    
    SLAVE2_SELECT = ~(sensor->sens_conf.SPI_ACTIVE);
    
}  


SENSOR_STATE New_Sensor(uint8_t channel,uint8_t *HeadPtr, uint8_t *TailPrt){
     
    
    read_sens_object.object_ptr = &sensors[channel];
    SENSOR_OBJECT *sens = read_sens_object.object_ptr;
   
    sens->sens_conf.state = SENSOR_DISCONNECTED;
    
    DeleteData(channel);
    ParseData(sens, HeadPtr, TailPrt,channel);
    
    if(parse_status == PARSE_DONE){
       sens->sens_conf.state = SENSOR_CONNECTED; 
       
       // write config files into EEprom if needed HERE !
    }
    else{
       sens->sens_conf.state = SENSOR_FAILED;
    }
    
    
    
    return sens->sens_conf.state;
}


void DeleteData(uint8_t channel){
    uint8_t i = 0;
 
    for (i=0;i<sizeof(sensor_mem[channel].all_data);i++){
      sensor_mem[channel].all_data[i] = 0x01;    // delete all data
    }
   
    for (i=0;i<sizeof(sensors[channel].EEsensor);i++){   
        sensors[channel].EEsensor[i] = 0x01;   // delete all config 
    }
 
  
}

void ParseData(SENSOR_OBJECT *sensor,uint8_t *HeadPtr, uint8_t *TailPrt,uint8_t channel){
    
    uint8_t sens = *TailPrt++;
    uint8_t readout = *(TailPrt++)+sens;
    uint8_t config = *(TailPrt++)+readout;
    uint8_t count = 0;
    
    sensor->sens_conf.data_prt.config_str_ptr = sensor_mem[channel].sensor_eed.config_data_str;
    sensor->sens_conf.data_prt.readout_str_ptr = sensor_mem[channel].sensor_eed.readout_data_str;
    
    
    while(TailPrt < (HeadPtr+1)){  // should wait for parse done and do by uart or usb interrupt !!!
        if(TailPrt < (HeadPtr+1)){
            
            if(count<sens){
                sensor->EEsensor[count] = *TailPrt++;
                count++;
            }
            else if(count<readout){
                *(sensor->sens_conf.data_prt.readout_str_ptr++) = *TailPrt++;
                count++;
            }
            else if(count<config){
                *(sensor->sens_conf.data_prt.config_str_ptr++) = *TailPrt++;
                count++;
            }
              
}
}
    sensor->sens_conf.data_prt.config_str_ptr = sensor_mem[channel].sensor_eed.config_data_str;
    sensor->sens_conf.data_prt.readout_str_ptr = sensor_mem[channel].sensor_eed.readout_data_str; 
    sensor->sens_conf.data_prt.readout_ptr = sensor_mem[channel].readout_data;
    
}

/**
 End of File
*/

/**
     case 0x43:
            SPI2_Exchange8bit(*(sensor->data_prt->config_str_ptr++));
            SPI2_Exchange8bitBuffer(uint8_t sensor->data_prt->config_ptr, uint8_t *(sensor->data_prt->config_str_ptr), NULL);
            sensor->data_prt->readout_ptr += *(sensor->data_prt->readout_str_ptr++);
            break;
            
        case 0x44:
            SPI2_Exchange8bit(*(sensor->data_prt->readout_str_ptr++));
            sensor->data_prt->readout_ptr++ = SPI2_Exchange8bit(DUMMY_DATA); 
            break;   
 * 
 * */