/**
  TMR7 Generated Driver File

  @Company
    Microchip Technology Inc.

  @File Name
    tmr7.c

  @Summary
    This is the generated driver implementation file for the TMR7 driver using MPLAB(c) Code Configurator

  @Description
    This source file provides APIs for TMR7.
    Generation Information :
        Product Revision  :  MPLAB(c) Code Configurator - 3.15.0
        Device            :  PIC18F67K22
        Driver Version    :  2.00
    The generated drivers are tested against the following:
        Compiler          :  XC8 1.35
        MPLAB             :  MPLAB X 3.20
*/

/*
    (c) 2016 Microchip Technology Inc. and its subsidiaries. You may use this
    software and any derivatives exclusively with Microchip products.

    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED
    WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A
    PARTICULAR PURPOSE, OR ITS INTERACTION WITH MICROCHIP PRODUCTS, COMBINATION
    WITH ANY OTHER PRODUCTS, OR USE IN ANY APPLICATION.

    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE,
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS
    BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO THE
    FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN
    ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY,
    THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.

    MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF THESE
    TERMS.
*/

/**
  Section: Included Files
*/

#include <xc.h>
#include "tmr7.h"

/**
  Section: Global Variables Definitions
*/
volatile uint16_t timer7ReloadVal;

/**
  Section: TMR7 APIs
*/

void TMR7_Initialize(void)
{
    //Set the Timer to the options selected in the GUI

    //T7CKPS 1:8; RD16 disabled; SOSCEN disabled; nT7SYNC synchronize; TMR7CS FOSC/4; TMR7ON enabled; 
    T7CON = 0x30;

    //T7GSS T7G; TMR7GE disabled; T7GTM disabled; T7GPOL low; T7GGO done; T7GSPM disabled; T7GVAL disabled; 
    T7GCON = 0x00;

    //TMR7H 224; 
    TMR7H = 0xE0;

    //TMR7L 0; 
    TMR7L = 0x00;

    // Load the TMR value to reload variable
    timer7ReloadVal=(TMR7H << 8) | TMR7L;

    // Clearing IF flag before enabling the interrupt.
    PIR5bits.TMR7IF = 0;

    // Enabling TMR7 interrupt.
    PIE5bits.TMR7IE = 1;

    // Set Default Interrupt Handler
    TMR7_SetInterruptHandler(TMR7_DefaultInterruptHandler);

    // Start TMR7
    //TMR7_StartTimer();
}

void TMR7_StartTimer(void)
{
    // Start the Timer by writing to TMRxON bit
    T7CONbits.TMR7ON = 1;
}

void TMR7_StopTimer(void)
{
    // Stop the Timer by writing to TMRxON bit
    T7CONbits.TMR7ON = 0;
}

uint16_t TMR7_ReadTimer(void)
{
    uint16_t readVal;

    readVal = (TMR7H << 8) | TMR7L;

    return readVal;
}

void TMR7_WriteTimer(uint16_t timerVal)
{
    if (T7CONbits.nT7SYNC == 1)
    {
        // Stop the Timer by writing to TMRxON bit
        T7CONbits.TMR7ON = 0;

        // Write to the Timer7 register
        TMR7H = (timerVal >> 8);
        TMR7L = timerVal;

        // Start the Timer after writing to the register
        //T7CONbits.TMR7ON =1;
    }
    else
    {
        // Write to the Timer7 register
        TMR7H = (timerVal >> 8);
        TMR7L = timerVal;
    }
}

void TMR7_Reload(void)
{
    //Write to the Timer7 register
    TMR7H = (timer7ReloadVal >> 8);
    TMR7L = timer7ReloadVal;
}

void TMR7_StartSinglePulseAcquisition(void)
{
    T7GCONbits.T7GGO = 1;
}

uint8_t TMR7_CheckGateValueStatus(void)
{
    return (T7GCONbits.T7GVAL);
}

void TMR7_ISR(void)
{

    // Clear the TMR7 interrupt flag
    PIR5bits.TMR7IF = 0;

    TMR7H = (timer7ReloadVal >> 8);
    TMR7L = timer7ReloadVal;

    if(TMR7_InterruptHandler)
    {
        TMR7_InterruptHandler();
    }
}


void TMR7_SetInterruptHandler(void* InterruptHandler){
    TMR7_InterruptHandler = InterruptHandler;
}

void TMR7_DefaultInterruptHandler(void){
    // add your TMR7 interrupt custom code
    // or set custom function using TMR7_SetInterruptHandler()
}

/**
  End of File
*/
