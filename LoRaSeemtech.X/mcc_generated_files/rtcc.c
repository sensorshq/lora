
/**
  RTCC Generated Driver API Header File

  @Company:
    Microchip Technology Inc.

  @File Name:
    rtcc.c

  @Summary:
    This is the generated header file for the RTCC driver using MPLAB(c) Code Configurator

  @Description:
    This header file provides APIs for driver for RTCC.
    Generation Information :
        Product Revision  :  MPLAB(c) Code Configurator - 3.15.0
        Device            :  PIC18F67K22
        Driver Version    :  0.5
    The generated drivers are tested against the following:
        Compiler          :  XC8 1.35
        MPLAB 	          :  MPLAB X 3.20
*/

/*
    (c) 2016 Microchip Technology Inc. and its subsidiaries. You may use this
    software and any derivatives exclusively with Microchip products.

    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED
    WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A
    PARTICULAR PURPOSE, OR ITS INTERACTION WITH MICROCHIP PRODUCTS, COMBINATION
    WITH ANY OTHER PRODUCTS, OR USE IN ANY APPLICATION.

    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE,
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS
    BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO THE
    FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN
    ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY,
    THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.

    MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF THESE
    TERMS.
*/


/**
 Section: Included Files
*/

#include <xc.h>
#include "rtcc.h"

/**
* Section: Function Prototype
*/
static bool rtccTimeInitialized = false;
static bool RTCCTimeInitialized(void);

/**
* Section: Driver Interface Function Definitions
*/

void RTCC_Initialize(void)
{
   // Make sure to select the clock source for RTCC from config bits (default SOSC)


   // In order to be able to write the Write Enable(WREN) bit for RTCC you neet to enable EEPROM writing 
   // it is strongly recommended to disable interrupts during this code segment - see INTERRUPT_GlobalInterruptDisable() in interrupt manager.h if using interrupts
   EECON2 = 0x55;
   EECON2 = 0xAA;
   // Set the RTCWREN bit
   __builtin_write_RTCWREN();
   RTCCFGbits.RTCEN = 0;
   
   if(!RTCCTimeInitialized())
   {
       // set RTCC time 2016-07-18 16-19-46
        RTCCFGbits.RTCPTR = 3;        // start the sequence
        RTCVALL = 0x16;  // YEAR
        RTCVALH = 0x00;
        RTCVALL = 0x18;   // DAY
        RTCVALH = 0x7; // MONTH
        RTCVALL = 0x16;  // HOURS
        RTCVALH = 0x1;  // WEEKDAY
        RTCVALL = 0x46;   // SECONDS
        RTCVALH = 0x19;   // MINUTES
        rtccTimeInitialized = true;
   }

   // set Alarm time 2016-07-18 16-19-46
    ALRMCFGbits.ALRMEN = 0;
    ALRMCFGbits.ALRMPTR = 2;
    ALRMVALL = 0x18; // DAY
    ALRMVALH = 0x7;   // MONTH
    ALRMVALL = 0x16;    // HOURS
    ALRMVALH = 0x1;    // WEEKDAY
    ALRMVALL = 0x46; // SECONDS
    ALRMVALH = 0x19; // MINUTES

   // ALRMPTR WDAY_HOUR; AMASK Every Minute; CHIME enabled; ALRMEN enabled; 
   ALRMCFG = 0xCD;

   // RTSECSEL0 Alarm Pulse; 
   PADCFG1 = 0x00;

   // CAL 0;  calibration register
   RTCCAL = 0x00;
           
   // ARPT 255; , number of time alarm is repeated
   ALRMRPT = 0xFF;

   // Enable RTCC, clear RTCWREN
   RTCCFGbits.RTCEN = 1;
   EECON2 = 0x55;
   EECON2 = 0xAA;
   __builtin_clear_RTCWREN();

   //Enable RTCC interrupt
   PIE3bits.RTCCIE = 1;
}

/**
 This function implements RTCC_TimeReset.This function is used to
 used by application to reset the RTCC value and reinitialize RTCC value.
*/
void RTCC_TimeReset()
{
    rtccTimeInitialized = false;
}

static bool RTCCTimeInitialized(void)
{
    return(rtccTimeInitialized);
}

/**
 This function implements RTCC_TimeGet. It access the 
 registers of  RTCC and writes to them the values provided 
 in the function argument currentTime
*/

bool RTCC_TimeGet(struct tm *currentTime)
{
    uint8_t register_valueHigh;
    uint8_t register_valueLow;
    if(RTCCFGbits.RTCSYNC){
        return false;
    }

    EECON2 = 0x55;
    EECON2 = 0xAA;
   // Set the RTCWREN bit
   __builtin_write_RTCWREN();

    RTCCFGbits.RTCPTR = 3;
    register_valueLow = RTCVALL;
    register_valueHigh = RTCVALH;    // reading/writing the high byte will decrement the pointer value by 1
    currentTime->tm_year = ConvertBCDToHex(register_valueLow);
  //  RCFGCALbits.RTCPTR = 2;
    register_valueLow = RTCVALL;
    register_valueHigh = RTCVALH;
    currentTime->tm_mon = ConvertBCDToHex(register_valueHigh);
    currentTime->tm_mday = ConvertBCDToHex(register_valueLow);
  //  RCFGCALbits.RTCPTR = 1;
    register_valueLow = RTCVALL;
    register_valueHigh = RTCVALH;;
    currentTime->tm_wday = ConvertBCDToHex(register_valueHigh);
    currentTime->tm_hour = ConvertBCDToHex(register_valueLow);
  //  RCFGCALbits.RTCPTR = 0;
    register_valueLow = RTCVALL;
    register_valueHigh = RTCVALH;
    currentTime->tm_min = ConvertBCDToHex(register_valueHigh);
    currentTime->tm_sec = ConvertBCDToHex(register_valueLow);

    // Clear RTCWREN Bit
    EECON2 = 0x55;
    EECON2 = 0xAA;
    __builtin_clear_RTCWREN();
    
    return true;
}

/**
 * This function sets the RTCC value and takes the input time in decimal format
*/

void RTCC_TimeSet(struct tm *initialTime)
{
    // Set the RTCWREN bit
    EECON2 = 0x55;
    EECON2 = 0xAA;
    __builtin_write_RTCWREN();

    RTCCFGbits.RTCEN = 0;
   
    PIR3bits.RTCCIF = 0;
    PIE3bits.RTCCIE = 0;

    // set RTCC initial time
    RTCCFGbits.RTCPTR = 3;                               // start the sequence
    RTCVALL =  ConvertHexToBCD(initialTime->tm_year);                        // YEAR
    RTCVALH = 0x00;
    RTCVALL = ConvertHexToBCD(initialTime->tm_mday);                         // DAY-1
    RTCVALH = ConvertHexToBCD(initialTime->tm_mon);                          // Month-1
    RTCVALL = ConvertHexToBCD(initialTime->tm_hour);                         // HOURS
    RTCVALH = ConvertHexToBCD(initialTime->tm_wday);                         // WEEKDAY
    RTCVALL = ConvertHexToBCD(initialTime->tm_sec);                          // SECONDS
    RTCVALH = ConvertHexToBCD(initialTime->tm_min);                          // MINUTES
             
    // Enable RTCC, clear RTCWREN         
    RTCCFGbits.RTCEN = 1;  
    EECON2 = 0x55;
    EECON2 = 0xAA;
    __builtin_clear_RTCWREN();
    
    PIE3bits.RTCCIE = 1;

}

/**
 This function reads the RTCC time and returns the date and time in BCD format
  */
bool RTCC_BCDTimeGet(bcdTime_t *currentTime)
{
    uint8_t register_valueHigh;
    uint8_t register_valueLow;
    if(RTCCFGbits.RTCSYNC){
        return false;
    }
    
    // Set the RTCWREN bit
    EECON2 = 0x55;
    EECON2 = 0xAA;
    __builtin_write_RTCWREN();

    RTCCFGbits.RTCPTR = 3;
    register_valueLow = RTCVALL;
    register_valueHigh = RTCVALH;
    currentTime->tm_year = register_valueLow;
    //  RCFGCALbits.RTCPTR = 2;
    register_valueLow = RTCVALL;
    register_valueHigh = RTCVALH;
    currentTime->tm_mon = register_valueHigh;
    currentTime->tm_mday = register_valueLow;
    //  RCFGCALbits.RTCPTR = 1;
    register_valueLow = RTCVALL;
    register_valueHigh = RTCVALH;;
    currentTime->tm_wday = register_valueHigh;
    currentTime->tm_hour = register_valueLow;
    //  RCFGCALbits.RTCPTR = 0;
    register_valueLow = RTCVALL;
    register_valueHigh = RTCVALH;
    currentTime->tm_min = register_valueHigh;
    currentTime->tm_sec = register_valueLow;

    // Clear RTCWREN Bit
    EECON2 = 0x55;
    EECON2 = 0xAA;
    __builtin_clear_RTCWREN();

   return true;
}
/**
 This function takes the input date and time in BCD format and sets the RTCC
 */
void RTCC_BCDTimeSet(bcdTime_t *initialTime)
{
    // Set the RTCWREN bit
    EECON2 = 0x55;
    EECON2 = 0xAA;
    __builtin_write_RTCWREN();

    RTCCFGbits.RTCEN = 0; 

    PIR3bits.RTCCIF = 0;
    PIE3bits.RTCCIE = 0;

    // set RTCC initial time
    RTCCFGbits.RTCPTR = 3;                               // start the sequence
    RTCVALL =  initialTime->tm_year;                        // YEAR
    RTCVALH = 0x00;
    RTCVALL = initialTime->tm_mday;                         // DAY-1
    RTCVALH = initialTime->tm_mon;                          // Month-1
    RTCVALL = initialTime->tm_hour;                         // HOURS
    RTCVALH = initialTime->tm_wday;                         // WEEKDAY
    RTCVALL = initialTime->tm_sec;                          // SECONDS
    RTCVALH = initialTime->tm_min;                          // MINUTES
    
    // Enable RTCC, clear RTCWREN
    RTCCFGbits.RTCEN = 1;  
    EECON2 = 0x55;
    EECON2 = 0xAA;
    __builtin_clear_RTCWREN();

    PIE3bits.RTCCIE = 1;

}

uint8_t ConvertHexToBCD(uint8_t hexvalue)
{
    uint8_t bcdvalue;
    bcdvalue = (hexvalue / 10) << 4;
    bcdvalue = bcdvalue | (hexvalue % 10);
    return (bcdvalue);
}

uint8_t ConvertBCDToHex(uint8_t bcdvalue)
{
    uint8_t hexvalue;
    hexvalue = (((bcdvalue & 0xF0) >> 4)* 10) + (bcdvalue & 0x0F);
    return hexvalue;
}


time_t ConvertDateTimeToUnixTime(struct tm *tmTime)
{
    return mktime(tmTime);
}

struct tm *ConvertUnixTimeToDateTime(time_t *unixTime)
{
    return gmtime(unixTime);
}

void RTCC_ISR(void)
{
    /* TODO : Add interrupt handling code */
    PIR3bits.RTCCIF = 0;
}


/**
 End of File
*/
