/* Microchip Technology Inc. and its subsidiaries.  You may use this software 
 * and any derivatives exclusively with Microchip products. 
 * 
 * THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS".  NO WARRANTIES, WHETHER 
 * EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED 
 * WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A 
 * PARTICULAR PURPOSE, OR ITS INTERACTION WITH MICROCHIP PRODUCTS, COMBINATION 
 * WITH ANY OTHER PRODUCTS, OR USE IN ANY APPLICATION. 
 *
 * IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, 
 * INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND 
 * WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS 
 * BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE.  TO THE 
 * FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS 
 * IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF 
 * ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.
 *
 * MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF THESE 
 * TERMS. 
 */

/* 
 * File: sensors.h
 * Author: Jure Macerl
 * Comments: This file supports sensor switching and provide basic linked list structure for sensor.c
 * Revision history: 1.0
 */

// This is a guard condition so that contents of this file are not included
// more than once.  
#ifndef XC_HEADER_TEMPLATE_H
#define	XC_HEADER_TEMPLATE_H


#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>

 // include processor files - each processor file is guarded.  

// TODO Insert appropriate #include <>

// TODO Insert C++ class definitions if appropriate

// TODO Insert declarations

// Comment a function and leverage automatic documentation with slash star star
/**
    <p><b>Summary:</b></p>

    <p><b>Description:</b></p>

    <p><b>Precondition:</b></p>

    <p><b>Parameters:</b></p>

    <p><b>Returns:</b></p>

    <p><b>Example:</b></p>
    <code>
 
    </code>

    <p><b>Remarks:</b></p>
 */
// TODO Insert declarations or function prototypes (right here) to leverage 

#define SENSOR_NUMBER   2
#define CONFIG_STR_LENGTH 50
#define READOUT_STR_LENGTH 50

#define READOUT_SIZE_PAGE 30
// live documentation

#ifdef	__cplusplus
extern "C" {
#endif /* __cplusplus */

    // TODO If C++ is being used, regular C code needs function names to have C 
    // linkage so the functions can be used by the c code. 
    
typedef struct {
    uint8_t *readout_ptr;
    uint8_t *config_str_ptr;
    uint8_t *readout_str_ptr;
    
}SENSOR_DATA;    
    



typedef enum {
    SENSOR_WORKING,
    SENSOR_SLEEPING,
    SENSOR_CONNECTED,
    SENSOR_FAILED,
    SENSOR_DISCONNECTED
}SENSOR_STATE;
    

/**
 Declaration of Sensor data for RAM and EEprom
 
 */
typedef struct {
    uint8_t config_data_str [CONFIG_STR_LENGTH];
    uint8_t readout_data_str [READOUT_STR_LENGTH];
} SENSOR_EED;



typedef union {

struct {

    union {
        SENSOR_EED sensor_eed;
        uint8_t SENSOR_EEData[sizeof (SENSOR_EED)]; // Alias: This will be stored in EEPROM so can be started on power up
    };

    union {
        uint8_t readout_data [READOUT_SIZE_PAGE];
        uint8_t SENSOR_RAMData[READOUT_SIZE_PAGE]; // Alias: should be stored in ram, because data are read from sensor and sended
    };
};
uint8_t all_data[sizeof (SENSOR_EED) + READOUT_SIZE_PAGE]; // Alias: all data for easier acces

} SENSOR_MEM;
   


/**
 Sensor object descriptor
 
 */



   typedef struct {             
        uint8_t ID; // SENSOR ID

        union {

            struct { // PROTOCOL DESCRIPTION BITS
                unsigned PP : 2;
                unsigned CH : 3;
                unsigned    : 3;
            };
            uint8_t COMMUNICATION;
        };

        uint8_t WAIT; // WAIT TIME BEFORE DATA IS AVAILABLE ON SENSOR
        uint8_t FREQ; // READOUT FREQUENCY

        union {

            struct { // I2C SPECIFIC DATA
                unsigned ACK : 1;
                unsigned S : 1;
                unsigned P : 1;
                unsigned RS_W : 1;
                unsigned RS_R : 1;
                unsigned : 3;
            };

            struct { // SPI CONFIGURATION DATA
                unsigned CPOL : 1;
                unsigned CPHA : 1;
                unsigned SMP : 1;
                unsigned SPI_ACTIVE : 1;
                unsigned : 4;
            };

        };

        struct { // DEFAULT PROTOCOL SPECIFIC DATA
            unsigned : 8;
        };
        SENSOR_STATE state;
        SENSOR_DATA data_prt;
    }SENS_CONF;


typedef  union {

      SENS_CONF sens_conf; // sensor configuration
      uint8_t EEsensor[sizeof(SENS_CONF)]; // Alias to store all the data to EEprom

    }SENSOR_OBJECT; 

    
/**
 * Current read state sensors. If returned read state is READ_DONE than sensor is ok. Otherwise sensor will
 * be put into SENSOR_FAILED and information will be sended to master on Rasp Pi.
 */  
    typedef enum{
        READ_PENDING,
        READ_DONE,
        READ_FAILED
    }READ_STATE;
 
    
    
/**

 Pointer to sensors object and count of stored in memory

 */
    typedef struct {
        
        uint8_t count;   // number of sensors connected
        SENSOR_OBJECT *object_ptr;
        
    }READ_SENS_OBJECT;
    
/**
 * Parse status when changing sensors. If incoming data can't be parsed properly or data size and 
 * length doesn't match NACK will be send to master on Rasp Pi.
 */

typedef enum{
    PARSE_DONE,
    PARSE_PENDING,
    PARSE_FAILED          
}PARSE_STATUS;     
    
    
    
uint8_t SensorRead(void); // Should be called when timer times out.

READ_STATE SENSOR_GET_DATA_SPI(SENSOR_OBJECT *sensor);
void SPI_CHANGE_STATE(SENSOR_OBJECT *sensor);   

SENSOR_STATE SENSOR_GET_DATA_I2C(SENSOR_OBJECT *sensor);

/**
 * Functions bellow used when attaching new sensor
 */
SENSOR_STATE New_Sensor(uint8_t channel,uint8_t *HeadPtr, uint8_t *TailPrt);
void DeleteData(uint8_t channel);
void ParseData(SENSOR_OBJECT *sensor,uint8_t *HeadPtr, uint8_t *TailPrt,uint8_t channel);
    
    
    
    
#ifdef	__cplusplus
}
#endif /* __cplusplus */

#endif	/* XC_HEADER_TEMPLATE_H */

