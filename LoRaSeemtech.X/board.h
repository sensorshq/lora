

/* 
 * File: board.h
 * Author: Jure Macerl
 * Comments: Basic definition for board with RF_SOLUTIONS LoRa board
 * Revision history: 1.0
 */


#ifndef __BOARD_H__
#define __BOARD_H__

#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>




/*!
 * NULL definition
 */
#ifndef NULL
    #define NULL                                    ( ( void * )0 )
#endif

/*!
 * Generic definition
 */
#ifndef SUCCESS
#define SUCCESS                                     1
#endif

#ifndef FAIL
#define FAIL                                        0  
#endif


/*!
 * Unique Devices IDs register set ( STM32L1xxx )
 */
#define         ID1                                 ( 0x1FF80050 )
#define         ID2                                 ( 0x1FF80054 )
#define         ID3                                 ( 0x1FF80064 )

/*!
 * Random seed generated using the MCU Unique ID
 */
#define RAND_SEED                                   ( ( *( uint32_t* )ID1 ) ^ \
                                                      ( *( uint32_t* )ID2 ) ^ \
                                                      ( *( uint32_t* )ID3 ) )


#define RADIO_ANT_SWITCH_RX                         LATBbits.LATB0
#define RADIO_ANT_SWITCH_TX                         LATBbits.LATB1
#define AntTx                                       TRISBbits.TRISB0
#define AntRx                                       TRISBbits.TRISB1



void BoardGetUniqueId( uint8_t *id );

#endif // __BOARD_H__
